#!/bin/bash

set -euo pipefail

main() {
    exec terraform "$@"
}

main $@
