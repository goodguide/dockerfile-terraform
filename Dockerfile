FROM quay.io/goodguide/base:alpine-3.6

RUN apk --update add \
      bash \
      curl \
      git \
      ncurses \
      openssh \
      zip

RUN version=0.11.1 shasum='4e3d5e4c6a267e31e9f95d4c1b00f5a7be5a319698f0370825b459cb786e2f35' \
 && cd /tmp \
 && curl -L -o ./terraform.zip "https://releases.hashicorp.com/terraform/${version}/terraform_${version}_linux_amd64.zip" \
 && sha256sum ./terraform.zip | tee /dev/stderr | grep -q "${shasum}" \
 && unzip ./terraform.zip \
 && install ./terraform /usr/local/bin/

RUN mkdir -p /root/.terraform.d/plugin-cache
COPY ./entrypoint.sh /bin/_entrypoint
COPY ./terraformrc /root/.terraformrc
ENTRYPOINT ["/sbin/tini", "-g", "--", "/bin/_entrypoint"]

VOLUME /terraform
VOLUME /root/.terraform.d/plugin-cache
WORKDIR /terraform

CMD ["help"]
