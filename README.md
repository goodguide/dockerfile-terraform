# Dockerfile-Terraform

## Priority/Importance

Vital to production deployment pipeline. It is required by gg-nova and to perform any deployments to http://www.goodguide.com

## Purpose

This is the docker image used by gg-nova to ensure everyone uses the exact same Terraform version.

## Automated Image Build

There is a GitLab deploy hook the triggers a Docker image build at Quay.io: https://quay.io/repository/goodguide/terraform?tab=info

To pull this image into docker:

```
docker pull quay.io/goodguide/terraform:VERSION
```

(where `VERSION` is one of the git tags which are named `v*.*.*`)
